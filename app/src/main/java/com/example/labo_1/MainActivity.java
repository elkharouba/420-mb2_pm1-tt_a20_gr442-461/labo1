package com.example.labo_1;

import androidx.appcompat.app.AppCompatActivity;

import android.content.ContentValues;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import java.util.ArrayList;

public class MainActivity extends AppCompatActivity {
    DBManager db;
    EditText nom ;
    EditText prenom;
    EditText email;
    EditText user;
    EditText password;
    EditText solde;
    EditText credit;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);
        this.db= new DBManager(this);
        this.nom = (EditText) findViewById(R.id.nom);
        this.prenom = (EditText) findViewById(R.id.prenom);
        this.user = (EditText) findViewById(R.id.user);
        this.password = (EditText) findViewById(R.id.password);
        this.email = (EditText) findViewById(R.id.email);
        this.solde = (EditText) findViewById(R.id.solde);
        this.credit = (EditText) findViewById(R.id.credit);

    }

    public void SavedData(View view) {
        ContentValues values = new ContentValues();

        values.put("name",this.nom.getText().toString());
        values.put("lname",this.prenom.getText().toString());
        values.put("email",this.email.getText().toString());
        values.put("user",this.user.getText().toString());
        values.put("solde",this.solde.getText().toString());
        values.put("password",this.password.getText().toString());
        values.put("credit",this.credit.getText().toString());
        long res = db.insert(values);

        if(res > 0)
        {
            this.nom.setText("");
            this.prenom.setText("");
            this.email.setText("");
            this.user.setText("");
            this.password.setText("");
            this.solde.setText("");
            this.credit.setText("");
            Toast.makeText(getApplicationContext(),"data inserted !",Toast.LENGTH_LONG).show();

        }



    }


    // Load data to ListView

    public void LoadData(View view) {

        ArrayList<Client>    listnewsData = new ArrayList<Client>();
        MyCustomAdapter myadapter;

        listnewsData = db.SelectAllStudent();
        for(Client list:db.SelectAllStudent())
        {
            System.out.println("Nom : "+list.nom);
            System.out.println("Prenom :"+list.prenom);
        }
        myadapter=new MyCustomAdapter(listnewsData);
        ListView lsNews=(ListView)findViewById(R.id.ListView);
        lsNews.setAdapter(myadapter);


    }

    public  void SearchByName(View view)
    {
        Client client =  db.selectByName(nom.getText().toString());
        System.out.println(client.prenom);
        prenom.setText(client.prenom);
        email.setText(client.email);
        user.setText(client.user);
        password.setText(client.password);
        nom.setText(client.nom);
        solde.setText(client.solde);
        credit.setText(client.credit);

    }



    //display news list
    private class MyCustomAdapter extends BaseAdapter {
        public ArrayList<Client> listnewsDataAdpater ;

        public MyCustomAdapter(ArrayList<Client>  listnewsDataAdpater) {
            this.listnewsDataAdpater=listnewsDataAdpater;
        }


        @Override
        public int getCount() {
            return listnewsDataAdpater.size();
        }

        @Override
        public String getItem(int position) {
            return null;
        }

        @Override
        public long getItemId(int position) {
            return position;
        }

        @Override
        public View getView(int position, View convertView, ViewGroup parent)
        {
            LayoutInflater mInflater = getLayoutInflater();
            View myView = mInflater.inflate(R.layout.layout_listview, null);

            final   Client s = listnewsDataAdpater.get(position);

            TextView list_nom =( TextView)myView.findViewById(R.id.list_nom);
            list_nom.setText(s.nom);

            TextView list_prenom =( TextView)myView.findViewById(R.id.list_prenom);
            list_prenom.setText(s.prenom);

            TextView list_solde =( TextView)myView.findViewById(R.id.list_solde);
            list_solde.setText(s.solde+"$");

            TextView list_credit =( TextView)myView.findViewById(R.id.list_credit);
            list_credit.setText(s.credit+"$");



            return myView;
        }

    }
}