package com.example.labo_1;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.TextView;
import android.widget.Toast;

import java.sql.Struct;
import java.util.ArrayList;

public class DBManager {

    private SQLiteDatabase sqLiteDatabase;
    static final  String DBName = "dbClient";
    static final int DBVersion = 1;
    static final String TableName = "client";

    static final String CreateTable = "create table if not exists "+TableName+"(id integer primary key autoincrement,name text,lname text,email text,user text,password text,solde text,credit text)";

    static  class  DataBaseHelperUser extends SQLiteOpenHelper
    {
        public Context context ;
        public  DataBaseHelperUser(Context context)
        {
            super(context,DBName,null,DBVersion);
            this.context = context;
        }

        @Override
        public void onCreate(SQLiteDatabase db) {
            db.execSQL(CreateTable);
            Toast.makeText(context,"Database created !",Toast.LENGTH_LONG).show();
        }

        @Override
        public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion) {
            db.execSQL("DROP TABLE IF EXISTS "+TableName);
            onCreate(db);
        }
    }

    public DBManager(Context context)
    {
        DataBaseHelperUser DbHelper = new DataBaseHelperUser(context);
        sqLiteDatabase = DbHelper.getWritableDatabase();
    }

    // Insert data
    public long insert(ContentValues values)
    {
        long id =  sqLiteDatabase.insert(TableName,"",values);
        return id;
    }


    public  int delete(String id)
    {
        String[] list = {id};
        int count = this.sqLiteDatabase.delete(TableName,"id = ?",list);
        return  count;
    }


    // select all data
    public ArrayList<Client> SelectAllStudent()
    {
        ArrayList<Client> listOfClient = new ArrayList<Client>();

        Cursor cursor = sqLiteDatabase.rawQuery("select * from "+TableName,null);

        if( (cursor !=null) && cursor.moveToFirst())
        {
            do{
                    listOfClient.add(new Client(cursor.getInt(0),cursor.getString(1),cursor.getString(2),cursor.getString(3),cursor.getString(4),cursor.getString(5),cursor.getString(6),cursor.getString(7)));
            }while(cursor.moveToNext());
        }


        return  listOfClient;
    }

    //Select by Name
    public Client selectByName(String nom)
    {
        Client client = null ;
        String[] list = {nom};
        Cursor cursor = sqLiteDatabase.rawQuery("select * from "+TableName+" where name = ?", list);

        if( (cursor !=null) && cursor.moveToFirst())
        {
            do{
                client = new Client(cursor.getInt(0),cursor.getString(1),cursor.getString(2),cursor.getString(3),cursor.getString(4),cursor.getString(5),cursor.getString(6),cursor.getString(7));
            }
            while(cursor.moveToNext());
        }

        return  client;
    }




}



