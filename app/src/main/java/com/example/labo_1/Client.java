package com.example.labo_1;

public class Client {

    public  int ID;
    public  String nom;
    public  String prenom;
    public  String email;
    public  String user;
    public  String password;
    public  String solde;
    public  String credit;

    Client()
    {

    }
    Client(int id , String nom , String prenom , String email , String user , String password , String solde , String credit)
    {
        this.ID = id ;
        this.nom = nom;
        this.prenom = prenom;
        this.email = email;
        this.user = user;
        this.password = password;
        this.solde = solde;
        this.credit = credit;
    }

    public int getID() {
        return ID;
    }

    public void setID(int ID) {
        this.ID = ID;
    }

    public String getNom() {
        return nom;
    }

    public void setNom(String nom) {
        this.nom = nom;
    }

    public String getPrenom() {
        return prenom;
    }

    public void setPrenom(String prenom) {
        this.prenom = prenom;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getUser() {
        return user;
    }

    public void setUser(String user) {
        this.user = user;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getSolde() {
        return solde;
    }

    public void setSolde(String solde) {
        this.solde = solde;
    }

    public String getCredit() {
        return credit;
    }

    public void setCredit(String credit) {
        this.credit = credit;
    }
}
